Modele.php (PHP uniquement) pour l'accès aux données ;
vueAccueil.php (PHP et HTML) pour l'affichage des billets du blog ;
index.php (PHP uniquement) pour faire le lien entre les deux pages précédentes.

//////////////////////////////////////////////////////////////////////////////////////////

La partie Modèle d'une architecture MVC encapsule la logique métier (« business logic ») ainsi que l'accès aux données. Il peut s'agir d'un ensemble de fonctions (Modèle procédural) ou de classes (Modèle orienté objet).

La partie Vue s'occupe des interactions avec l'utilisateur : présentation, saisie et validation des données.

La partie Contrôleur gère la dynamique de l'application. Elle fait le lien entre l'utilisateur et le reste de l'application.

///////////////////////////////////////////////////////////////////////////////////////////

Modele.php représente la partie Modèle (accès aux données) ;
vueAccueil.php, vueBillet.php et vueErreur.php constituent la partie Vue (affichage à l'utilisateur). Ces pages utilisent la page gabarit.php (template de mise en forme commune) ;
index.php et billet.php correspondent à la partie Contrôleur (gestion des requêtes entrantes).

///////////////////////////////////////////////////////////////////////////////////////////

Le contrôleur frontal analyse la requête entrante et vérifie les paramètres fournis ;
Il sélectionne et appelle l'action à réaliser en lui passant les paramètres nécessaires ;
Si la requête est incohérente, il signale l'erreur à l'utilisateur.

///////////////////////////////////////////////////////////////////////////////////////////

le répertoire Modele contiendra le fichier Modele.php ;
le répertoire Vue contiendra les fichiers vueAccueil.php, vueBillet.php et vueErreur.php, ainsi que la page commune gabarit.php ;
le répertoire Controleur contiendra le fichier des actions Controleur.php.

///////////////////////////////////////////////////////////////////////////////////////////

une classe se compose d'attributs et de méthodes ;
le mot-clé class permet de définir une classe ;
les différents niveaux d'accessibilité sont public, protected et private ;
le mot-clé extends permet de définir une classe dérivée (comme en Java) ;
le mot-clé $this permet d'accéder aux membres de l'objet courant.

///////////////////////////////////////////////////////////////////////////////////////////

PHP étant un langage à typage dynamique, on ne précise pas les types des attributs et des méthodes, mais seulement leur niveau d'accessibilité ;
le mot-clé function permet de déclarer une méthode, quelle que soit sa valeur de retour ;
le mot-clé parent permet d'accéder au parent de l'objet courant. Il joue en PHP le même rôle que base en C# et super en Java ;
le constructeur d'une classe s'écrit __construct ;
la méthode __toString détermine comment l'objet est affiché en tant que chaîne de caractères ;
on peut redéfinir (override) une méthode, comme ici __toString, sans mot-clé particulier ;
le mot-clé $this est obligatoire pour accéder aux membres de l'objet courant. Son utilisation est optionnelle en C# et en Java, et souvent limitée à la levée des ambiguïtés entre attributs et paramètres ;
il est possible de définir une valeur par défaut pour les paramètres d'une méthode. Elle est utilisée lorsque l'argument (paramètre effectif) n'est pas précisé au moment de l'appel.

///////////////////////////////////////////////////////////////////////////////////////////

ajout ou enrichissement de la classe modèle associée ;
ajout ou enrichissement d'une vue utilisant le gabarit pour afficher les données ;
ajout ou enrichissement d'une classe contrôleur pour lier le modèle et la vue.

///////////////////////////////////////////////////////////////////////////////////////////



_once // Déclare qu'une fois

IV-B-2